rdt
===

Spit out the titles and links for the front page of a given subreddit,
including specifying "rising", "new" etc.

Just two horrible little shell scripts...but they work!

* `rdt`
* `rdt programming`
* `rdt programming rising`
* `rdtm programming linux vim`

Requires [`curl`][1], [`pup`][2], and [`jq(1)`][3].

License
-------

Copyright (c) [Tom Ryder][4]. Distributed under an [MIT License][5].

[1]: https://curl.haxx.se/
[2]: https://github.com/ericchiang/pup
[3]: https://stedolan.github.io/jq/
[4]: https://sanctum.geek.nz/
[5]: https://www.opensource.org/licenses/MIT
